# <a href="https://tripetto.com/"><img src="https://unpkg.com/tripetto/assets/banner.svg" alt="Tripetto"></a>

Tripetto is a full-fledged form kit. Rapidly build and run smart flowing forms and surveys. Drop the kit in your codebase and use all of it or just the parts you need. The visual [**builder**](https://www.npmjs.com/package/tripetto) is for form building, and the [**runners**](https://www.npmjs.com/package/tripetto-runner-foundation) are for running those forms in different UI variants. It is entirely extendible and customizable. Anyone can build their own building [**blocks**](https://docs.tripetto.com/guide/blocks) (e.g., question types) or runner UI's.

# Variable block
[![Status](https://gitlab.com/tripetto/blocks/variable/badges/master/pipeline.svg)](https://gitlab.com/tripetto/blocks/variable/commits/master)
[![Version](https://img.shields.io/npm/v/tripetto-block-variable.svg)](https://www.npmjs.com/package/tripetto-block-variable)
[![License](https://img.shields.io/npm/l/tripetto-block-variable.svg)](https://opensource.org/licenses/MIT)
[![Downloads](https://img.shields.io/npm/dt/tripetto-block-variable.svg)](https://www.npmjs.com/package/tripetto-block-variable)
[![Follow us on Twitter](https://img.shields.io/twitter/follow/tripetto.svg?style=social&label=Follow)](https://twitter.com/tripetto)

Use this block to store custom variables inside Tripetto forms. It is a headless block, so it doesn't require a UI implementation for the runner.

# Get started

You need to install or import this block to use it in Tripetto. If you are using the CLI version of the builder, you can find instructions [here](https://docs.tripetto.com/guide/builder/#cli-configuration). If you are embedding the builder into your own project using the library, take a look [here](https://docs.tripetto.com/guide/builder/#library-blocks).

# Use cases

- Directly in your browser (add `<script src="https://unpkg.com/tripetto-block-variable"></script>` to your HTML);
- In the CLI builder (install the block using `npm i tripetto-block-variable -g` and update your Tripetto [config](https://docs.tripetto.com/guide/builder/#cli-configuration));
- In your builder implementation (just add `import "tripetto-block-variable";` to your code);
- In your runner implementation (simply add `import "tripetto-block-variable/runner";` to your code.

# Support

Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/blocks/variable/issues) and we'll look into them.

For general support contact us at [support@tripetto.com](mailto:support@tripetto.com). We're more than happy to assist you.

# License

Have a blast. [MIT](https://opensource.org/licenses/MIT).

# Contributors
- [Hisam A Fahri](https://gitlab.com/hisamafahri) (Indonesian translation)

# About us

If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
