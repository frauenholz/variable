/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    HeadlessBlock,
    Slots,
    Value,
    assert,
    isFilledString,
    tripetto,
} from "tripetto-runner-foundation";
import "./boolean";
import "./date";
import "./numeric";
import "./text";

/**
 * This block has a dependency on the calculator block. We need to activate a
 * separate namespace for it to guard against version conflicts when another
 * block depends on another version of the calculator block.
 */
import "./namespace/mount";
import { IOperation, calculator } from "tripetto-block-calculator/runner";
import "./namespace/unmount";

@tripetto({
    type: "headless",
    identifier: PACKAGE_NAME,
})
export class Variable extends HeadlessBlock<{
    readonly prefill?: string | number | boolean;
    readonly calculator?: true | undefined;
    readonly operations?: IOperation[];
}> {
    readonly variableSlot = assert(
        this.valueOf("variable", "static", {
            modifier: (value) =>
                (this.props.calculator &&
                    this.props.operations &&
                    value.slot instanceof Slots.Numeric && {
                        value: calculator(
                            this.context,
                            this.props.operations,
                            value as Value<number, Slots.Numeric>,
                            undefined,
                            (id) => this.variableFor(id),
                            (id) => this.parseVariables(id)
                        ),
                    }) ||
                undefined,
        })
    );

    do() {
        this.variableSlot.reset(
            isFilledString(this.props.prefill)
                ? this.parseVariables(this.props.prefill, "", true)
                : this.props.prefill
        );
    }
}
