/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    castToBoolean,
    condition,
    isFilledString,
    tripetto,
} from "tripetto-runner-foundation";
import { TMode } from "./mode";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:boolean`,
})
export class VariableBooleanCondition extends ConditionBlock<{
    readonly mode: TMode;
    readonly value?: string;
}> {
    @condition
    verify(): boolean {
        const booleanSlot = this.valueOf<boolean>();

        if (booleanSlot) {
            switch (this.props.mode) {
                case "true":
                    return (
                        booleanSlot.hasValue && castToBoolean(booleanSlot.value)
                    );
                case "false":
                    return (
                        booleanSlot.hasValue &&
                        !castToBoolean(booleanSlot.value)
                    );
                case "equal":
                case "not-equal":
                    const value =
                        (isFilledString(this.props.value) &&
                            this.variableFor(this.props.value)) ||
                        undefined;
                    return (
                        (value &&
                            (booleanSlot.hasValue
                                ? castToBoolean(booleanSlot.value)
                                : undefined) ===
                                (value.hasValue
                                    ? castToBoolean(value.value)
                                    : undefined)) ||
                        false
                    );
                case "defined":
                    return booleanSlot.hasValue;
                case "undefined":
                    return !booleanSlot.hasValue;
            }
        }

        return false;
    }
}
