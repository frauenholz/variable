export type TMode =
    | "true"
    | "false"
    | "equal"
    | "not-equal"
    | "defined"
    | "undefined";
