/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

import {
    Collection,
    DateTime,
    Forms,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    insertVariable,
    isBoolean,
    isDefined,
    isNumber,
    isNumberFinite,
    isString,
    pgettext,
    renamed,
    slots,
    tripetto,
} from "tripetto";
import { VariableTextCondition } from "./conditions/text";
import { VariableNumericCondition } from "./conditions/numeric";
import { VariableDateCondition } from "./conditions/date";
import { VariableBooleanCondition } from "./conditions/boolean";
import { TMode as TTextMode } from "../runner/text/mode";
import { TMode as TNumericMode } from "../runner/numeric/mode";
import { TMode as TDateMode } from "../runner/date/mode";
import { TMode as TBooleanMode } from "../runner/boolean/mode";

/**
 * This block has a dependency on the calculator block. We need to activate a
 * separate namespace for it to guard against version conflicts when another
 * block depends on another version of the calculator block.
 */
import "./namespace/mount";
import { ICalculator, Operation, calculator } from "tripetto-block-calculator";
import "./namespace/unmount";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_BOOLEAN from "../../assets/boolean.svg";
import ICON_DATE from "../../assets/date.svg";
import ICON_NUMERIC from "../../assets/numeric.svg";
import ICON_TEXT from "../../assets/text.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    kind: "headless",
    icon: ICON,
    get label() {
        return pgettext("block:variable", "Custom variable");
    },
})
export class Variable extends NodeBlock implements ICalculator {
    readonly allowMarkdown = false;
    readonly startBlank = false;
    currentType: "text" | "numeric" | "boolean" | "date" | undefined;
    slot!: Slots.Text | Slots.Numeric | Slots.Boolean | Slots.Date;

    @definition
    prefill?: string | number | boolean;

    @definition
    calculator?: true | undefined;

    @definition
    readonly operations: Collection.Provider<Operation, ICalculator> =
        Collection.of<Operation, ICalculator>(Operation, this);

    get block() {
        return this;
    }

    get firstANS() {
        return "@" + this.slot.id;
    }

    get variableType(): "text" | "numeric" | "boolean" | "date" {
        return this.currentType || "text";
    }

    @affects("#slots")
    @affects("#label")
    set variableType(type: "text" | "numeric" | "boolean" | "date") {
        this.currentType = type;
    }

    get icon() {
        switch (this.currentType) {
            case "numeric":
                return ICON_NUMERIC;
            case "boolean":
                return ICON_BOOLEAN;
            case "date":
                return ICON_DATE;
            case "text":
                return ICON_TEXT;
            default:
                return ICON;
        }
    }

    get label() {
        switch (this.currentType) {
            case "numeric":
                return pgettext(
                    "block:variable",
                    "%1 (number)",
                    this.type.label
                );
            case "boolean":
                return pgettext(
                    "block:variable",
                    "%1 (boolean)",
                    this.type.label
                );
            case "date":
                return this.slot instanceof Slots.Date && this.slot.supportsTime
                    ? pgettext(
                          "block:variable",
                          "%1 (date/time)",
                          this.type.label
                      )
                    : pgettext("block:variable", "%1 (date)", this.type.label);
            default:
                return pgettext("block:variable", "%1 (text)", this.type.label);
        }
    }

    private createProxy(
        refs: () => {
            textValue?: Forms.Text;
            numericValue?: Forms.Numeric;
            dateValue?: Forms.DateTime;
            booleanValue?: Forms.Radiobutton<true | false>;
        }
    ) {
        const ref = this;

        return {
            get maxLength() {
                return ref.slot instanceof Slots.Text
                    ? ref.slot.maxLength
                    : undefined;
            },
            set maxLength(value: number | undefined) {
                if (ref.slot instanceof Slots.Text) {
                    ref.slot.maxLength = value;
                }

                refs().textValue?.maxLength(value || 0);
            },
            get transformation() {
                return ref.slot instanceof Slots.Text
                    ? ref.slot.transformation
                    : undefined;
            },
            set transformation(value: Slots.Transformations | undefined) {
                if (ref.slot instanceof Slots.Text) {
                    ref.slot.transformation = value;
                }

                refs().textValue?.transformation(value || "none");
            },
            get numericPrecision() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.precision
                    : undefined;
            },
            set numericPrecision(value: number | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.precision = value;
                }

                refs().numericValue?.precision(value || 0);
            },
            get digits() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.digits
                    : undefined;
            },
            set digits(value: number | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.digits = value;
                }

                refs().numericValue?.digits(value || 0);
            },
            get decimal() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.decimal
                    : undefined;
            },
            set decimal(value: string | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.decimal = value;
                }

                refs().numericValue?.decimalSign(value || "");
            },
            get separator() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.separator
                    : undefined;
            },
            set separator(value: string | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.separator = value;
                }
                refs().numericValue?.thousands(
                    value ? true : false,
                    value || ""
                );
            },
            get numericMinimum() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.minimum
                    : undefined;
            },
            set numericMinimum(value: number | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.minimum = value;
                }

                refs().numericValue?.min(value);
            },
            get numericMaximum() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.maximum
                    : undefined;
            },
            set numericMaximum(value: number | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.maximum = value;
                }

                refs().numericValue?.max(value);
            },
            get prefix() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.prefix
                    : undefined;
            },
            set prefix(value: string | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.prefix = value;
                }

                refs().numericValue?.prefix(value || "");
            },
            get prefixPlural() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.prefixPlural
                    : undefined;
            },
            set prefixPlural(value: string | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.prefixPlural = value;
                }

                refs().numericValue?.prefixPlural(value || undefined);
            },
            get suffix() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.suffix
                    : undefined;
            },
            set suffix(value: string | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.suffix = value;
                }

                refs().numericValue?.suffix(value || "");
            },
            get suffixPlural() {
                return ref.slot instanceof Slots.Numeric
                    ? ref.slot.suffixPlural
                    : undefined;
            },
            set suffixPlural(value: string | undefined) {
                if (ref.slot instanceof Slots.Numeric) {
                    ref.slot.suffixPlural = value;
                }

                refs().numericValue?.suffixPlural(value || undefined);
            },
            get time() {
                return ref.slot instanceof Slots.Date
                    ? ref.slot.supportsTime || undefined
                    : undefined;
            },
            set time(value: boolean | undefined) {
                if (ref.slot instanceof Slots.Date) {
                    ref.slot.precision = value === true ? "seconds" : "days";
                    ref.rerender(true);
                }

                refs().dateValue?.features(
                    Forms.DateTimeFeatures.Date |
                        (value === true
                            ? Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                            : Forms.DateTimeFeatures.Weekday)
                );
            },
            get dateMinimum() {
                return ref.slot instanceof Slots.Date
                    ? ref.slot.minimum
                    : undefined;
            },
            set dateMinimum(value: number | true | Date | undefined) {
                if (ref.slot instanceof Slots.Date) {
                    ref.slot.minimum = value;
                }
            },
            get dateMaximum() {
                return ref.slot instanceof Slots.Date
                    ? ref.slot.maximum
                    : undefined;
            },
            set dateMaximum(value: number | true | Date | undefined) {
                if (ref.slot instanceof Slots.Date) {
                    ref.slot.maximum = value;
                }
            },
            get labelForFalse() {
                return ref.slot instanceof Slots.Boolean
                    ? ref.slot.labelForFalse
                    : undefined;
            },
            set labelForFalse(value: string | undefined) {
                if (ref.slot instanceof Slots.Boolean) {
                    ref.slot.labelForFalse = value;
                }

                refs().booleanValue?.buttonLabel(
                    false,
                    value || pgettext("block:variable", "False")
                );
            },
            get labelForTrue() {
                return ref.slot instanceof Slots.Boolean
                    ? ref.slot.labelForTrue
                    : undefined;
            },
            set labelForTrue(value: string | undefined) {
                if (ref.slot instanceof Slots.Boolean) {
                    ref.slot.labelForTrue = value;
                }

                refs().booleanValue?.buttonLabel(
                    true,
                    value || pgettext("block:variable", "True")
                );
            },
        };
    }

    @slots
    @renamed
    defineSlot(): void {
        const slot = this.slots.select("variable", "static");

        if (!this.currentType) {
            if (slot instanceof Slots.Numeric) {
                this.currentType = "numeric";
            } else if (slot instanceof Slots.Boolean) {
                this.currentType = "boolean";
            } else if (slot instanceof Slots.Date) {
                this.currentType = "date";
            } else {
                this.currentType = "text";
            }
        }

        switch (this.variableType) {
            case "text":
                this.slot = this.slots.static({
                    type: Slots.Text,
                    reference: "variable",
                    label:
                        this.node.name ||
                        pgettext("block:variable", "Variable"),
                    exchange: [
                        "alias",
                        "exportable",
                        "transformation",
                        "maxLength",
                    ],
                    alias: slot?.alias,
                    exportable: slot?.exportable,
                });
                break;
            case "numeric":
                this.slot = this.slots.static({
                    type: Slots.Numeric,
                    reference: "variable",
                    label:
                        this.node.name ||
                        pgettext("block:variable", "Variable"),
                    exchange: [
                        "alias",
                        "exportable",
                        "precision",
                        "digits",
                        "decimal",
                        "separator",
                        "minimum",
                        "maximum",
                        "prefix",
                        "prefixPlural",
                        "suffix",
                        "suffixPlural",
                    ],
                    alias: slot?.alias,
                    exportable: slot?.exportable,
                });
                break;
            case "boolean":
                this.slot = this.slots.static({
                    type: Slots.Boolean,
                    reference: "variable",
                    label:
                        this.node.name ||
                        pgettext("block:variable", "Variable"),
                    exchange: [
                        "alias",
                        "exportable",
                        "labelForTrue",
                        "labelForFalse",
                    ],
                    alias: slot?.alias,
                    exportable: slot?.exportable,
                });
                break;
            case "date":
                this.slot = this.slots.static({
                    type: Slots.Date,
                    reference: "variable",
                    label:
                        this.node.name ||
                        pgettext("block:variable", "Variable"),
                    exchange: [
                        "alias",
                        "exportable",
                        "precision",
                        "minimum",
                        "maximum",
                    ],
                    alias: slot?.alias,
                    exportable: slot?.exportable,
                });

                if (!this.slot.precision) {
                    this.slot.precision = "days";
                }
                break;
        }
    }

    @editor
    defineEditor(): void {
        const proxy = this.createProxy(() => ({
            textValue,
            numericValue,
            dateValue,
            booleanValue,
        }));

        this.editor.form({
            title: pgettext("block:variable", "Explanation"),
            controls: [
                new Forms.Static(
                    pgettext(
                        "block:variable",
                        "Creates a custom variable you can use to store a value in."
                    )
                ).markdown(),
            ],
        });

        this.editor.name(
            false,
            false,
            pgettext("block:variable", "Name"),
            false
        );

        this.editor.option({
            name: pgettext("block:variable", "Type"),
            locked: true,
            form: {
                title: pgettext("block:variable", "Type"),
                controls: [
                    new Forms.Radiobutton<
                        "text" | "numeric" | "boolean" | "date"
                    >(
                        [
                            {
                                label: pgettext("block:variable", "Text"),
                                description: pgettext(
                                    "block:variable",
                                    "Store a text value."
                                ),
                                value: "text",
                            },
                            {
                                label: pgettext("block:variable", "Number"),
                                description: pgettext(
                                    "block:variable",
                                    "Store a numeric value."
                                ),
                                value: "numeric",
                            },
                            {
                                label: pgettext("block:variable", "Date"),
                                description: pgettext(
                                    "block:variable",
                                    "Store a date value with an optional time."
                                ),
                                value: "date",
                            },
                            {
                                label: pgettext("block:variable", "Boolean"),
                                description: pgettext(
                                    "block:variable",
                                    "Store true or false."
                                ),
                                value: "boolean",
                            },
                        ],
                        Forms.Radiobutton.bind(this, "variableType", "text")
                    ).on((type) => {
                        each(textFeatures, (feature) => {
                            feature.disabled(type.value !== "text");
                            feature.visible(type.value === "text");
                        });

                        each(numericFeatures, (feature) => {
                            feature.disabled(type.value !== "numeric");
                            feature.visible(type.value === "numeric");
                        });

                        each(dateFeatures, (feature) => {
                            feature.disabled(type.value !== "date");
                            feature.visible(type.value === "date");
                        });

                        each(booleanFeatures, (feature) => {
                            feature.disabled(type.value !== "boolean");
                            feature.visible(type.value === "boolean");
                        });

                        textValue.visible(type.value === "text");
                        numericValue.visible(type.value === "numeric");
                        dateValue.visible(type.value === "date");
                        booleanValue.visible(type.value === "boolean");
                        calculatorFeature.disabled(type.value !== "numeric");
                        calculatorFeature.visible(type.value === "numeric");
                    }),
                ],
            },
        });

        this.editor.groups.settings();

        const textFeatures = [
            this.editor.option({
                name: pgettext("block:variable", "Limits"),
                form: {
                    title: pgettext(
                        "block:variable",
                        "Maximum number of characters"
                    ),
                    controls: [
                        new Forms.Numeric(
                            Forms.Numeric.bind(proxy, "maxLength", undefined)
                        ).min(0),
                    ],
                },
                activated: isNumber(proxy.maxLength),
                visible: this.variableType === "text",
                disabled: this.variableType !== "text",
            }),
            this.editor
                .transformations(proxy)
                .visible(this.variableType === "text")
                .disabled(this.variableType !== "text"),
        ];

        const numericMinimum = new Forms.Numeric(
            Forms.Numeric.bind(proxy, "numericMinimum", undefined)
        )
            .precision(proxy.numericPrecision || 0)
            .digits(proxy.digits || 0)
            .decimalSign(proxy.decimal || "")
            .thousands(proxy.separator ? true : false, proxy.separator || "")
            .prefix(proxy.prefix || "")
            .prefixPlural(proxy.prefixPlural || undefined)
            .suffix(proxy.suffix || "")
            .suffixPlural(proxy.suffixPlural || undefined)
            .label(pgettext("block:variable", "Minimum"));
        const numericMaximum = new Forms.Numeric(
            Forms.Numeric.bind(proxy, "numericMaximum", undefined)
        )
            .precision(proxy.numericPrecision || 0)
            .digits(proxy.digits || 0)
            .decimalSign(proxy.decimal || "")
            .thousands(proxy.separator ? true : false, proxy.separator || "")
            .prefix(proxy.prefix || "")
            .prefixPlural(proxy.prefixPlural || undefined)
            .suffix(proxy.suffix || "")
            .suffixPlural(proxy.suffixPlural || undefined)
            .label(pgettext("block:variable", "Maximum"));

        const numericPrefix = new Forms.Text(
            "singleline",
            Forms.Text.bind(proxy, "prefix", undefined)
        )
            .sanitize(false)
            .on((p: Forms.Text) => {
                numericMinimum.prefix((p.isFeatureEnabled && p.value) || "");
                numericMaximum.prefix((p.isFeatureEnabled && p.value) || "");
            });
        const numericPluralPrefix = new Forms.Text(
            "singleline",
            Forms.Text.bind(proxy, "prefixPlural", undefined)
        )
            .indent(32)
            .sanitize(false)
            .on((p: Forms.Text) => {
                numericMinimum.prefixPlural(
                    (p.isFeatureEnabled && p.isObservable && p.value) ||
                        undefined
                );
                numericMaximum.prefixPlural(
                    (p.isFeatureEnabled && p.isObservable && p.value) ||
                        undefined
                );
            })
            .placeholder(
                pgettext("block:variable", "Prefix when value is plural")
            )
            .visible(isString(proxy.prefixPlural));

        const numericSuffix = new Forms.Text(
            "singleline",
            Forms.Text.bind(proxy, "suffix", undefined)
        )
            .sanitize(false)
            .on((s: Forms.Text) => {
                numericMinimum.suffix((s.isFeatureEnabled && s.value) || "");
                numericMaximum.suffix((s.isFeatureEnabled && s.value) || "");
            });
        const numericPluralSuffix = new Forms.Text(
            "singleline",
            Forms.Text.bind(proxy, "suffixPlural", undefined)
        )
            .indent(32)
            .sanitize(false)
            .on((s: Forms.Text) => {
                numericMinimum.suffixPlural(
                    (s.isFeatureEnabled && s.isObservable && s.value) ||
                        undefined
                );
                numericMaximum.suffixPlural(
                    (s.isFeatureEnabled && s.isObservable && s.value) ||
                        undefined
                );
            })
            .placeholder(
                pgettext("block:variable", "Suffix when value is plural")
            )
            .visible(isString(proxy.suffixPlural));

        const numericDecimal = new Forms.Dropdown(
            [
                { label: "#.#", value: "." },
                { label: "#,#", value: "," },
            ],
            Forms.Dropdown.bind(proxy, "decimal", undefined)
        )
            .label(pgettext("block:variable", "Decimal sign"))
            .disabled(!proxy.numericPrecision)
            .on((decimal) => {
                numericMinimum.decimalSign(
                    (decimal.isFeatureEnabled && decimal.value) || ""
                );
                numericMaximum.decimalSign(
                    (decimal.isFeatureEnabled && decimal.value) || ""
                );
            });

        const numericFormat = this.editor.option({
            name: pgettext("block:variable", "Format"),
            form: {
                title: pgettext("block:variable", "Format"),
                controls: [
                    new Forms.Dropdown(
                        [
                            {
                                optGroup: pgettext(
                                    "block:variable",
                                    "Decimals"
                                ),
                            },
                            { label: "#", value: 0 },
                            { label: "#.#", value: 1 },
                            { label: "#.##", value: 2 },
                            { label: "#.###", value: 3 },
                            { label: "#.####", value: 4 },
                            { label: "#.#####", value: 5 },
                            { label: "#.######", value: 6 },
                            { label: "#.#######", value: 7 },
                            { label: "#.########", value: 8 },
                            {
                                optGroup: pgettext("block:variable", "Digits"),
                            },
                            { label: "##", value: -2 },
                            { label: "###", value: -3 },
                            { label: "####", value: -4 },
                            { label: "#####", value: -5 },
                            { label: "######", value: -6 },
                            { label: "#######", value: -7 },
                            { label: "########", value: -8 },
                            { label: "#########", value: -9 },
                            { label: "##########", value: -10 },
                            { label: "###########", value: -11 },
                            { label: "############", value: -12 },
                            { label: "#############", value: -13 },
                            { label: "##############", value: -14 },
                            { label: "###############", value: -15 },
                            { label: "################", value: -16 },
                        ],
                        (proxy.digits
                            ? -proxy.digits
                            : proxy.numericPrecision) || 0
                    ).on((format: Forms.Dropdown<number>) => {
                        proxy.numericPrecision =
                            format.isFeatureEnabled &&
                            isNumber(format.value) &&
                            format.value >= 0
                                ? format.value
                                : undefined;
                        proxy.digits =
                            format.isFeatureEnabled &&
                            isNumber(format.value) &&
                            format.value < 0
                                ? -format.value
                                : undefined;

                        numericMinimum.precision(proxy.numericPrecision || 0);
                        numericMaximum.precision(proxy.numericPrecision || 0);
                        numericMinimum.digits(proxy.digits || 0);
                        numericMaximum.digits(proxy.digits || 0);
                        numericDecimal.disabled(!proxy.numericPrecision);
                        numericSigns.disabled((proxy.digits || 0) > 0);
                    }),
                ],
            },
            activated:
                isNumber(proxy.numericPrecision) || isNumber(proxy.digits),
            visible: this.variableType === "numeric",
            disabled: this.variableType !== "numeric",
        });

        const numericFeatures = [
            numericFormat,
            this.editor.option({
                name: pgettext("block:variable", "Limits"),
                form: {
                    title: pgettext("block:variable", "Limits"),
                    controls: [numericMinimum, numericMaximum],
                },
                activated:
                    isNumber(proxy.numericMinimum) ||
                    isNumber(proxy.numericMaximum),
                visible: this.variableType === "numeric",
                disabled: this.variableType !== "numeric",
            }),
            this.editor.option({
                name: pgettext("block:variable", "Prefix"),
                form: {
                    title: pgettext("block:variable", "Prefix"),
                    controls: [
                        numericPrefix,
                        new Forms.Checkbox(
                            pgettext(
                                "block:variable",
                                "Specify different prefix for plural values"
                            ),
                            isString(proxy.prefixPlural)
                        ).on((c) => {
                            numericPrefix.placeholder(
                                (c.isChecked &&
                                    pgettext(
                                        "block:variable",
                                        "Prefix when value is singular"
                                    )) ||
                                    ""
                            );

                            numericPluralPrefix.visible(c.isChecked);
                        }),
                        numericPluralPrefix,
                    ],
                },
                activated: isString(proxy.prefix),
                visible: this.variableType === "numeric",
                disabled: this.variableType !== "numeric",
            }),
            this.editor.option({
                name: pgettext("block:variable", "Suffix"),
                form: {
                    title: pgettext("block:variable", "Suffix"),
                    controls: [
                        numericSuffix,
                        new Forms.Checkbox(
                            pgettext(
                                "block:variable",
                                "Specify different suffix for plural values"
                            ),
                            isString(proxy.suffixPlural)
                        ).on((c) => {
                            numericSuffix.placeholder(
                                (c.isChecked &&
                                    pgettext(
                                        "block:variable",
                                        "Suffix when value is singular"
                                    )) ||
                                    ""
                            );

                            numericPluralSuffix.visible(c.isChecked);
                        }),
                        numericPluralSuffix,
                    ],
                },
                activated: isString(proxy.suffix),
                visible: this.variableType === "numeric",
                disabled: this.variableType !== "numeric",
            }),
        ];

        const numericSigns = this.editor.option({
            name: pgettext("block:variable", "Signs"),
            form: {
                title: pgettext("block:variable", "Signs"),
                controls: [
                    numericDecimal,
                    new Forms.Dropdown(
                        [
                            {
                                label: pgettext("block:variable", "None"),
                                value: undefined,
                            },
                            { label: "#,###", value: "," },
                            { label: "#.###", value: "." },
                        ],
                        Forms.Dropdown.bind(proxy, "separator", undefined)
                    )
                        .label(
                            pgettext("block:variable", "Thousands separator")
                        )
                        .on((separator) => {
                            numericMinimum.thousands(
                                separator.isFeatureEnabled && separator.value
                                    ? true
                                    : false,
                                (separator.isFeatureEnabled &&
                                    separator.value) ||
                                    ""
                            );
                            numericMaximum.thousands(
                                separator.isFeatureEnabled && separator.value
                                    ? true
                                    : false,
                                (separator.isFeatureEnabled &&
                                    separator.value) ||
                                    ""
                            );
                        }),
                    new Forms.Static(
                        pgettext(
                            "block:variable",
                            "**Note:** These signs are used to format the number in de dataset. When the number is displayed in a runner, the appropriate user locale might be applied making it seem like changing these settings has no effect."
                        )
                    ).markdown(),
                ],
            },
            activated: isString(proxy.separator) || isString(proxy.decimal),
            visible: this.variableType === "numeric",
            disabled:
                this.variableType !== "numeric" || (proxy.digits || 0) > 0,
        });

        numericFeatures.push(numericSigns);

        const dateControlFeatures = (time?: boolean) =>
            Forms.DateTimeFeatures.Date |
            (time
                ? Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                : Forms.DateTimeFeatures.Weekday);

        const dateMinimum = new Forms.DateTime(
            isNumber(proxy.dateMinimum) ? proxy.dateMinimum : undefined
        )
            .zone("UTC")
            .features(dateControlFeatures(proxy.time))
            .disabled(proxy.dateMinimum === true)
            .years(
                new Date().getFullYear() - 150,
                new Date().getFullYear() + 50
            )
            .width("full")
            .label(pgettext("block:variable", "Minimum"))
            .placeholder(pgettext("block:variable", "Not set"))
            .on(() => {
                if (dateMinimum.isFeatureEnabled) {
                    if (proxy.dateMinimum !== true) {
                        proxy.dateMinimum = dateMinimum.value;
                    }
                } else {
                    proxy.dateMinimum = undefined;
                }
            });
        const dateMaximum = new Forms.DateTime(
            isNumber(proxy.dateMaximum) ? proxy.dateMaximum : undefined
        )
            .zone("UTC")
            .features(dateControlFeatures(proxy.time))
            .disabled(proxy.dateMaximum === true)
            .years(
                new Date().getFullYear() - 150,
                new Date().getFullYear() + 50
            )
            .width("full")
            .label(pgettext("block:variable", "Maximum"))
            .placeholder(pgettext("block:variable", "Not set"))
            .on(() => {
                if (dateMaximum.isFeatureEnabled) {
                    if (proxy.dateMaximum !== true) {
                        proxy.dateMaximum = dateMaximum.value;
                    }
                } else {
                    proxy.dateMaximum = undefined;
                }
            });

        const dateFeatures = [
            this.editor.option({
                name: pgettext("block:variable", "Time"),
                form: {
                    title: pgettext("block:variable", "Time"),
                    controls: [
                        new Forms.Checkbox(
                            pgettext(
                                "block:variable",
                                "Allow a time to be set"
                            ),
                            Forms.Checkbox.bind(proxy, "time", undefined, true)
                        ).on((time) => {
                            dateMinimum.features(
                                dateControlFeatures(
                                    time.isFeatureEnabled && time.isChecked
                                )
                            );
                            dateMaximum.features(
                                dateControlFeatures(
                                    time.isFeatureEnabled && time.isChecked
                                )
                            );
                        }),
                        new Forms.Static(
                            pgettext(
                                "block:variable",
                                "When enabled this will allow to set a specific time along with the date."
                            )
                        ),
                    ],
                },
                activated: isBoolean(proxy.time),
                visible: this.variableType === "date",
                disabled: this.variableType !== "date",
            }),
            this.editor.option({
                name: pgettext("block:variable", "Limits"),
                form: {
                    title: pgettext("block:variable", "Limits"),
                    controls: [
                        dateMinimum,
                        dateMaximum,
                        new Forms.Static(pgettext("block:variable", "Options")),
                        new Forms.Checkbox(
                            pgettext(
                                "block:variable",
                                "Date must be in the future"
                            ),
                            proxy.dateMinimum === true
                        ).on((future) => {
                            if (future.isFeatureEnabled && future.isChecked) {
                                proxy.dateMinimum = true;
                            } else if (proxy.dateMinimum === true) {
                                proxy.dateMinimum = undefined;

                                dateMinimum.refresh();
                            }

                            dateMinimum.disabled(proxy.dateMinimum === true);
                        }),
                        new Forms.Checkbox(
                            pgettext(
                                "block:variable",
                                "Date must be in the past"
                            ),
                            proxy.dateMaximum === true
                        ).on((past) => {
                            if (past.isFeatureEnabled && past.isChecked) {
                                proxy.dateMaximum = true;
                            } else if (proxy.dateMaximum === true) {
                                proxy.dateMaximum = undefined;

                                dateMaximum.refresh();
                            }

                            dateMaximum.disabled(proxy.dateMaximum === true);
                        }),
                    ],
                },
                activated:
                    isNumber(proxy.dateMinimum) ||
                    proxy.dateMinimum === true ||
                    isNumber(proxy.dateMaximum) ||
                    proxy.dateMaximum === true,
                visible: this.variableType === "date",
                disabled: this.variableType !== "date",
            }),
        ];

        const booleanFeatures = [
            this.editor.option({
                name: pgettext("block:variable", "Labels"),
                form: {
                    title: pgettext("block:variable", "Labels"),
                    controls: [
                        new Forms.Text(
                            "singleline",
                            Forms.Text.bind(proxy, "labelForTrue", undefined)
                        ).placeholder("True"),
                        new Forms.Text(
                            "singleline",
                            Forms.Text.bind(proxy, "labelForFalse", undefined)
                        ).placeholder("False"),
                        new Forms.Static(
                            pgettext(
                                "block:variable",
                                "These labels will be used in the dataset and override the default values %1 and %2.",
                                "**True**",
                                "**False**"
                            )
                        ).markdown(),
                    ],
                },
                activated:
                    isString(proxy.labelForFalse) ||
                    isString(proxy.labelForTrue),
                visible: this.variableType === "boolean",
                disabled: this.variableType !== "boolean",
            }),
        ];

        this.editor.groups.options();

        const textValue = new Forms.Text(
            "multiline",
            this.variableType === "text" && isString(this.prefill)
                ? this.prefill
                : undefined
        )
            .maxLength(proxy.maxLength || 0)
            .transformation(proxy.transformation || "none")
            .visible(this.variableType === "text")
            .action("@", insertVariable(this, "validated"))
            .on((t) => {
                if (t.isObservable) {
                    this.prefill = t.isFeatureEnabled ? t.value : undefined;
                }
            });

        const numericValue = new Forms.Numeric(
            this.variableType === "numeric" && isNumberFinite(this.prefill)
                ? this.prefill
                : undefined
        )
            .precision(proxy.numericPrecision || 0)
            .digits(proxy.digits || 0)
            .decimalSign(proxy.decimal || "")
            .thousands(proxy.separator ? true : false, proxy.separator || "")
            .prefix(proxy.prefix || "")
            .prefixPlural(proxy.prefixPlural || undefined)
            .suffix(proxy.suffix || "")
            .suffixPlural(proxy.suffixPlural || undefined)
            .min(proxy.numericMinimum)
            .max(proxy.numericMaximum)
            .visible(this.variableType === "numeric")
            .on((n) => {
                if (n.isObservable) {
                    this.prefill = n.isFeatureEnabled ? n.value : undefined;
                }
            });

        const dateValue = new Forms.DateTime(
            this.variableType === "date" && isNumberFinite(this.prefill)
                ? this.prefill
                : DateTime.UTCToday
        )
            .features(
                dateControlFeatures(
                    this.slot instanceof Slots.Date && this.slot.supportsTime
                )
            )
            .zone("UTC")
            .width("full")
            .required()
            .visible(this.variableType === "date")
            .on((d) => {
                if (d.isObservable) {
                    this.prefill = d.isFeatureEnabled ? d.value : undefined;
                }
            });

        const booleanValue = new Forms.Radiobutton<true | false>(
            [
                {
                    label:
                        (this.slot instanceof Slots.Boolean &&
                            this.slot.labelForTrue) ||
                        pgettext("block:variable", "True"),
                    value: true,
                },
                {
                    label:
                        (this.slot instanceof Slots.Boolean &&
                            this.slot.labelForFalse) ||
                        pgettext("block:variable", "False"),
                    value: false,
                },
            ],
            this.prefill !== false
        )
            .visible(this.variableType === "boolean")
            .on((b) => {
                if (b.isObservable) {
                    this.prefill = b.isFeatureEnabled ? b.value : undefined;
                }
            });

        this.editor.option({
            name: pgettext("block:variable", "Prefill"),
            form: {
                title: pgettext("block:variable", "Prefill"),
                controls: [
                    textValue,
                    numericValue,
                    dateValue,
                    booleanValue,
                    new Forms.Static(
                        pgettext(
                            "block:variable",
                            "This value will be used as initial (prefilled) value for the variable. It is only set once upon creation of the variable."
                        )
                    ),
                ],
            },
            activated: isDefined(this.prefill),
        });

        const calculatorFeature = this.editor
            .option({
                name: pgettext("block:variable", "Calculator"),
                collection: calculator(
                    this,
                    pgettext(
                        "block:variable",
                        "Perform calculation on variable value."
                    ),
                    pgettext("block:variable", "Calculator")
                ),
                activated: this.calculator === true,
                visible: this.variableType === "numeric",
            })
            .onToggle(() => {
                this.calculator = calculatorFeature.isActivated
                    ? true
                    : undefined;
            });

        this.editor.alias(() => this.slot);
        this.editor.exportable(() => this.slot);
    }

    @conditions
    defineCondition(): void {
        switch (this.variableType) {
            case "text":
                each(
                    [
                        {
                            mode: "exact",
                            label: pgettext("block:variable", "Text matches"),
                        },
                        {
                            mode: "not-exact",
                            label: pgettext(
                                "block:variable",
                                "Text does not match"
                            ),
                        },
                        {
                            mode: "contains",
                            label: pgettext("block:variable", "Text contains"),
                        },
                        {
                            mode: "not-contains",
                            label: pgettext(
                                "block:variable",
                                "Text does not contain"
                            ),
                        },
                        {
                            mode: "starts",
                            label: pgettext(
                                "block:variable",
                                "Text starts with"
                            ),
                        },
                        {
                            mode: "ends",
                            label: pgettext("block:variable", "Text ends with"),
                        },
                        {
                            mode: "defined",
                            label: pgettext(
                                "block:variable",
                                "Text is not empty"
                            ),
                        },
                        {
                            mode: "undefined",
                            label: pgettext("block:variable", "Text is empty"),
                        },
                    ],
                    (condition: { mode: TTextMode; label: string }) => {
                        this.conditions.template({
                            condition: VariableTextCondition,
                            label: condition.label,
                            autoOpen:
                                condition.mode !== "defined" &&
                                condition.mode !== "undefined",
                            props: {
                                slot: this.slot,
                                mode: condition.mode,
                            },
                        });
                    }
                );
                break;
            case "numeric":
                each(
                    [
                        {
                            mode: "equal",
                            label: pgettext(
                                "block:variable",
                                "Number is equal to"
                            ),
                        },
                        {
                            mode: "not-equal",
                            label: pgettext(
                                "block:variable",
                                "Number is not equal to"
                            ),
                        },
                        {
                            mode: "below",
                            label: pgettext(
                                "block:variable",
                                "Number is lower than"
                            ),
                        },
                        {
                            mode: "above",
                            label: pgettext(
                                "block:variable",
                                "Number is higher than"
                            ),
                        },
                        {
                            mode: "between",
                            label: pgettext(
                                "block:variable",
                                "Number is between"
                            ),
                        },
                        {
                            mode: "not-between",
                            label: pgettext(
                                "block:variable",
                                "Number is not between"
                            ),
                        },
                        {
                            mode: "defined",
                            label: pgettext(
                                "block:variable",
                                "Number is not empty"
                            ),
                        },
                        {
                            mode: "undefined",
                            label: pgettext(
                                "block:variable",
                                "Number is empty"
                            ),
                        },
                    ],
                    (condition: { mode: TNumericMode; label: string }) => {
                        this.conditions.template({
                            condition: VariableNumericCondition,
                            label: condition.label,
                            autoOpen:
                                condition.mode !== "defined" &&
                                condition.mode !== "undefined",
                            props: {
                                slot: this.slot,
                                mode: condition.mode,
                            },
                        });
                    }
                );
                break;
            case "date":
                each(
                    [
                        {
                            mode: "equal",
                            label: pgettext("block:variable", "Is equal to"),
                        },
                        {
                            mode: "not-equal",
                            label: pgettext(
                                "block:variable",
                                "Is not equal to"
                            ),
                        },
                        {
                            mode: "before",
                            label: pgettext("block:variable", "Is before"),
                        },
                        {
                            mode: "after",
                            label: pgettext("block:variable", "Is after"),
                        },
                        {
                            mode: "between",
                            label: pgettext("block:variable", "Is between"),
                        },
                        {
                            mode: "not-between",
                            label: pgettext("block:variable", "Is not between"),
                        },
                        {
                            mode: "defined",
                            label: pgettext("block:variable", "Is not empty"),
                        },
                        {
                            mode: "undefined",
                            label: pgettext("block:variable", "Is empty"),
                        },
                    ],
                    (condition: { mode: TDateMode; label: string }) => {
                        this.conditions.template({
                            condition: VariableDateCondition,
                            label: condition.label,
                            autoOpen:
                                condition.mode !== "defined" &&
                                condition.mode !== "undefined",
                            props: {
                                slot: this.slot,
                                mode: condition.mode,
                                value:
                                    this.slot instanceof Slots.Date &&
                                    this.slot.supportsTime
                                        ? DateTime.UTC
                                        : DateTime.UTCToday,
                                to:
                                    condition.mode === "between" ||
                                    condition.mode === "not-between"
                                        ? this.slot instanceof Slots.Date &&
                                          this.slot.supportsTime
                                            ? DateTime.UTC
                                            : DateTime.UTCToday
                                        : undefined,
                            },
                        });
                    }
                );
                break;
            case "boolean":
                each(
                    [
                        {
                            mode: "true",
                            label:
                                (this.slot instanceof Slots.Boolean &&
                                    this.slot.labelForTrue) ||
                                pgettext("block:variable", "Value is true"),
                        },
                        {
                            mode: "false",
                            label:
                                (this.slot instanceof Slots.Boolean &&
                                    this.slot.labelForFalse) ||
                                pgettext("block:variable", "Value is false"),
                        },
                        {
                            mode: "equal",
                            label: pgettext(
                                "block:variable",
                                "Value is equal to"
                            ),
                        },
                        {
                            mode: "not-equal",
                            label: pgettext(
                                "block:variable",
                                "Value is not equal to"
                            ),
                        },
                        {
                            mode: "defined",
                            label: pgettext(
                                "block:variable",
                                "Value is not empty"
                            ),
                        },
                        {
                            mode: "undefined",
                            label: pgettext("block:variable", "Value is empty"),
                        },
                    ],
                    (condition: { mode: TBooleanMode; label: string }) => {
                        this.conditions.template({
                            condition: VariableBooleanCondition,
                            label: condition.label,
                            autoOpen:
                                condition.mode === "equal" ||
                                condition.mode === "not-equal",
                            props: {
                                slot: this.slot,
                                mode: condition.mode,
                            },
                        });
                    }
                );
                break;
        }
    }
}
