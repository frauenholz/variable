/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Slots,
    affects,
    definition,
    editor,
    isVariable,
    lookupVariable,
    makeMarkdownSafe,
    pgettext,
    populateVariables,
    tripetto,
} from "tripetto";
import { TMode } from "../../runner/boolean/mode";

/** Assets */
import ICON from "../../../assets/boolean.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:boolean`,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:variable", "Verify value");
    },
})
export class VariableBooleanCondition extends ConditionBlock {
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: TMode = "true";

    @definition
    @affects("#name")
    value?: string;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name(): string {
        const slot = this.slot;

        if (slot instanceof Slots.Boolean) {
            switch (this.mode) {
                case "true":
                    return `@${slot.id} = ${
                        makeMarkdownSafe(slot.labelForTrue || "") ||
                        pgettext("block:variable", "True")
                    }`;
                case "false":
                    return `@${slot.id} = ${
                        makeMarkdownSafe(slot.labelForFalse || "") ||
                        pgettext("block:variable", "False")
                    }`;
                case "equal":
                case "not-equal":
                    return `@${slot.id} ${
                        this.mode === "not-equal" ? "\u2260" : "="
                    } ${
                        isVariable(this.value) &&
                        lookupVariable(this, this.value)?.label
                            ? `@${this.value}`
                            : "\\_\\_"
                    }`;
                case "defined":
                    return `@${slot.id} ${pgettext(
                        "block:variable",
                        "not empty"
                    )}`;
                case "undefined":
                    return `@${slot.id} ${pgettext("block:variable", "empty")}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:variable", "Compare mode"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label:
                                (this.slot instanceof Slots.Boolean &&
                                    this.slot.labelForTrue) ||
                                pgettext("block:variable", "Is true"),
                            value: "true",
                        },
                        {
                            label:
                                (this.slot instanceof Slots.Boolean &&
                                    this.slot.labelForFalse) ||
                                pgettext("block:variable", "Is false"),
                            value: "false",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Equals another variable"
                            ),
                            value: "equal",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Not equals another variable"
                            ),
                            value: "not-equal",
                        },
                        {
                            label: pgettext("block:variable", "Is not empty"),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:variable", "Is empty"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "true")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    if (mode.value !== "equal" && mode.value !== "not-equal") {
                        this.value = undefined;
                    }

                    form.visible(
                        mode.value === "equal" || mode.value === "not-equal"
                    );

                    switch (mode.value) {
                        case "equal":
                            form.title = pgettext(
                                "block:variable",
                                "If value matches"
                            );
                            break;
                        case "not-equal":
                            form.title = pgettext(
                                "block:variable",
                                "If value does not match"
                            );
                            break;
                    }
                }),
            ],
        });

        const isVar = (this.value && isVariable(this.value)) || false;
        const variables = populateVariables(
            this,
            (slot, pipe) => !pipe && slot instanceof Slots.Boolean,
            isVar ? this.value : undefined,
            false,
            this.slot?.id
        );
        const variableControl = new Forms.Dropdown(
            variables,
            isVar ? (this.value as string) : ""
        )
            .width("full")
            .on((variable) => {
                if (variable.isFormVisible && variable.isObservable) {
                    this.value = variable.value || undefined;
                }
            });

        const form = this.editor
            .form({
                title: pgettext("block:variable", "If value matches"),
                controls: [variableControl],
            })
            .visible(this.mode === "equal" || this.mode === "not-equal");
    }
}
