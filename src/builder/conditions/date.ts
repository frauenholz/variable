/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    DateTime,
    Forms,
    L10n,
    Slots,
    affects,
    definition,
    editor,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
    populateVariables,
    tripetto,
} from "tripetto";
import { TMode } from "../../runner/date/mode";
import { IVariableDateCondition } from "../../runner/date";

/** Assets */
import ICON from "../../../assets/date.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:date`,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:variable", "Verify date");
    },
})
export class VariableDateCondition
    extends ConditionBlock
    implements IVariableDateCondition
{
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: TMode = "equal";

    @definition
    @affects("#name")
    value?: number | string;

    @definition
    @affects("#name")
    to?: number | string;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot;

        if (slot instanceof Slots.Date) {
            const value = this.parse(slot, this.value);

            switch (this.mode) {
                case "between":
                    return `${value} ≤ @${slot.id} ≤ ${this.parse(
                        slot,
                        this.to
                    )}`;
                case "not-between":
                    return `@${slot.id} < ${value} ${pgettext(
                        "block:variable",
                        "or"
                    )} @${slot.id} > ${this.parse(slot, this.to)}`;
                case "defined":
                    return `@${slot.id} ${pgettext(
                        "block:variable",
                        "not empty"
                    )}`;
                case "undefined":
                    return `@${slot.id} ${pgettext("block:variable", "empty")}`;
                case "before":
                case "after":
                case "equal":
                case "not-equal":
                    return `@${slot.id} ${
                        this.mode === "after"
                            ? ">"
                            : this.mode === "before"
                            ? "<"
                            : this.mode === "not-equal"
                            ? "\u2260"
                            : "="
                    } ${value}`;
            }
        }

        return this.type.label;
    }

    private parse(
        slot: Slots.Date,
        value: number | string | undefined
    ): string {
        if (isNumberFinite(value)) {
            return slot.supportsTime
                ? L10n.locale.dateTimeShort(slot.toValue(value), true)
                : L10n.locale.dateShort(slot.toValue(value), true);
        } else if (isString(value)) {
            if (value && lookupVariable(this, value)?.label) {
                return `@${value}`;
            }

            return "\\_\\_";
        }

        return "`" + pgettext("block:variable", "Now").toUpperCase() + "`";
    }

    @editor
    defineEditor(): void {
        const supportsTime =
            this.slot instanceof Slots.Date && this.slot.supportsTime;

        this.editor.form({
            title: pgettext("block:variable", "Compare mode"),
            controls: [
                new Forms.Radiobutton<TMode>(
                    [
                        {
                            label: pgettext(
                                "block:variable",
                                "Date is equal to"
                            ),
                            value: "equal",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Date is not equal to"
                            ),
                            value: "not-equal",
                        },
                        {
                            label: pgettext("block:variable", "Date is before"),
                            value: "before",
                        },
                        {
                            label: pgettext("block:variable", "Date is after"),
                            value: "after",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Date is between"
                            ),
                            value: "between",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Date is not between"
                            ),
                            value: "not-between",
                        },
                        {
                            label: pgettext(
                                "block:variable",
                                "Date is not empty"
                            ),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:variable", "Date is empty"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on((mode: Forms.Radiobutton<TMode>) => {
                    from.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );
                    to.visible(
                        mode.value === "between" || mode.value === "not-between"
                    );

                    switch (mode.value) {
                        case "equal":
                            from.title = pgettext(
                                "block:variable",
                                "If date equals"
                            );
                            break;
                        case "not-equal":
                            from.title = pgettext(
                                "block:variable",
                                "If date not equals"
                            );
                            break;
                        case "before":
                            from.title = pgettext(
                                "block:variable",
                                "If date is before"
                            );
                            break;
                        case "after":
                            from.title = pgettext(
                                "block:variable",
                                "If date is after"
                            );
                            break;
                        case "between":
                            from.title = pgettext(
                                "block:variable",
                                "If date is between"
                            );
                            break;
                        case "not-between":
                            from.title = pgettext(
                                "block:variable",
                                "If date is not between"
                            );
                            break;
                    }
                }),
            ],
        });

        const addCondition = (
            property: "value" | "to",
            title: string,
            visible: boolean
        ) => {
            const value = this[property];
            const variables = populateVariables(
                this,
                (slot) => slot instanceof Slots.Date,
                isString(value) ? value : undefined,
                true,
                this.slot?.id
            );
            const dateControl = new Forms.DateTime(
                isNumberFinite(value)
                    ? value
                    : DateTime.UTCToday + (property === "to" ? 86400000 - 1 : 0)
            )
                .label(
                    supportsTime
                        ? pgettext("block:variable", "Use fixed date/time")
                        : pgettext("block:variable", "Use fixed date")
                )
                .features(
                    Forms.DateTimeFeatures.Date |
                        (supportsTime
                            ? Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                            : Forms.DateTimeFeatures.Weekday)
                )
                .years(
                    new Date().getFullYear() - 150,
                    new Date().getFullYear() + 50
                )
                .zone("UTC")
                .width("full")
                .required()
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        this[property] = input.value;
                    }
                });

            const variableControl = new Forms.Dropdown(
                variables,
                isString(value) ? value : ""
            )
                .label(pgettext("block:variable", "Use value of"))
                .width("full")
                .on((variable) => {
                    if (variable.isFormVisible && variable.isObservable) {
                        this[property] = variable.value || "";
                    }
                });

            return this.editor
                .form({
                    title,
                    controls: [
                        new Forms.Radiobutton<"current" | "variable" | "date">(
                            [
                                {
                                    label: supportsTime
                                        ? pgettext(
                                              "block:variable",
                                              "Current date/time"
                                          )
                                        : pgettext(
                                              "block:variable",
                                              "Current date"
                                          ),
                                    value: "current",
                                },
                                {
                                    label: supportsTime
                                        ? pgettext(
                                              "block:variable",
                                              "Fixed date/time"
                                          )
                                        : pgettext(
                                              "block:variable",
                                              "Fixed date"
                                          ),
                                    value: "date",
                                },
                                {
                                    label: pgettext("block:variable", "Value"),
                                    value: "variable",
                                    disabled: variables.length === 0,
                                },
                            ],
                            isString(value)
                                ? "variable"
                                : isNumberFinite(value)
                                ? "date"
                                : "current"
                        ).on((type) => {
                            dateControl.visible(type.value === "date");
                            variableControl.visible(type.value === "variable");

                            if (type.value === "current") {
                                this[property] = undefined;
                            }
                        }),
                        dateControl,
                        variableControl,
                    ],
                })
                .visible(visible);
        };

        const from = addCondition(
            "value",
            pgettext("block:variable", "If date equals"),
            this.mode !== "defined" && this.mode !== "undefined"
        );
        const to = addCondition(
            "to",
            pgettext("block:variable", "And"),
            this.mode === "between" || this.mode === "not-between"
        );
    }
}
